**tl;dr: If you change a reference to a collection then you will see a increase in performance otherwise Angular will handle  tracking itself.**

***
So what is a trackBy? According to documentation `An optional function passed into NgForOf that defines how to track items in an iterable (e.g. fby index or id)`. Pretty descriptive isn't it. 

## Example

Let's assume that we are CEO of superheroes team. We got a program that will get available heroes from API and print it.

This is our code: 

```html
<button (click)="change()">Call new heroes!</button>
<ul>
    <li *ngFor="let hero of heroes">{{ hero.name }}</li>
</ul>
```
```typescript
@Component({
    selector: 'app-withouttrack',
    templateUrl: './withouttrack.component.html',
    providers: [HeroService],
})
export class WithouttrackComponent implements OnInit {
    heroes: Hero[];
    constructor(private heroService: HeroService) {
        this.fetchHeroes();
    }
    ngOnInit() {}
    change() {
        this.fetchHeroes();
    }
    fetchHeroes() {
        this.heroService.getNewHeroes().subscribe(heroes => {
            this.heroes = heroes;
        });
    }
}

```
Stuff you do usually. You get data from service and print it using `*ngFor`.
Let's see it in action.

![No trackBy](https://i.imgur.com/GINXBhE.gif)

As you can see Angular rerenders every element even though only one changed. This may cause some serious performance problems. Rendering DOM is costly so let's locate cause of this issue.

### Problem
This is it.
```
 this.heroService.getNewHeroes().subscribe(heroes => {
            this.heroes = heroes;
});
```
Doing this Angular loses reference to collection and cannot track each objects. This cause framework to to "think" that all of object are new so they needed to be printed again.

### Solution
Here's where `trackBy` comes in handy.
Implementing it in application is really simple.

Change your HTML accordingly.
```html
<ul>
    <li *ngFor="let hero of heroes; trackBy: trackHero">{{ hero.name }}</li>
</ul>
```
In your Typescript file add tracking function. Tracking function always take two parameters `(index: number, item: any)`
 and returns `any`.
``` typescript
trackHero(index, item: Hero) {
    return item.id;
 }
```
Lets see what's changed.

![using trackby](https://i.imgur.com/h9srxTv.gif)

As you can see now Angular rerenders only single element that changed.

Problem fixed. 👍

Source code:
https://stackblitz.com/edit/angular-34tvgy?embed=1&file=src/index.html

***
This is my first post ever so I apolgize for any mistakes constructive criticism welcome. 🤞
Have a nice day.
