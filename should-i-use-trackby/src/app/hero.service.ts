import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { Hero } from './hero.model';

@Injectable()
export class HeroService {
    private oldHeroes = [
        { id: 1, name: 'Sausage man' },
        { id: 2, name: 'Dogman' },
        { id: 3, name: 'Airman' },
    ];
    private newHeroes = [
        { id: 1, name: 'Sausage man' },
        { id: 2, name: 'Dogman' },
        { id: 4, name: 'Red tiger' },
    ];

    private isFirstFetch = true;
    constructor() {}

    getNewHeroes(): Observable<Hero[]> {
        if (this.isFirstFetch) {
            this.isFirstFetch = false;
            return of(this.oldHeroes);
        }
        return of(this.newHeroes);
    }
}
