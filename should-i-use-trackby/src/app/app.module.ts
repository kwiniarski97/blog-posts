import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WithtrackComponent } from './withtrack/withtrack.component';
import { WithouttrackComponent } from './withouttrack/withouttrack.component';

@NgModule({
  declarations: [
    AppComponent,
    WithtrackComponent,
    WithouttrackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
