import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero.model';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-withouttrack',
    templateUrl: './withouttrack.component.html',
    providers: [HeroService],
})
export class WithouttrackComponent implements OnInit {
    heroes: Hero[];
    constructor(private heroService: HeroService) {
        this.fetchHeroes();
    }
    ngOnInit() {}
    change() {
        this.fetchHeroes();
    }
    fetchHeroes() {
        this.heroService.getNewHeroes().subscribe(heroes => {
            this.heroes = heroes;
        });
    }
}
