import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <button [routerLink]="['/trackby']">TrackBy</button>
        <button [routerLink]="['/nontrackby']">Not TrackBy</button>

        <router-outlet></router-outlet>
    `,
})
export class AppComponent {}
