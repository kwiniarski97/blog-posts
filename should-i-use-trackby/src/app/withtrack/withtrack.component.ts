import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero.model';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-withtrack',
    templateUrl: './withtrack.component.html',
    providers: [HeroService]
})
export class WithtrackComponent implements OnInit {
    heroes: Hero[];

    constructor(private heroService: HeroService) {
        this.fetchHeroes();
    }

    ngOnInit() {}

    change() {
        this.fetchHeroes();
    }

    trackHero(index, item: Hero) {
        return item.id;
    }

    fetchHeroes() {
        this.heroService.getNewHeroes().subscribe(heroes => {
            this.heroes = heroes;
        });
    }
}
