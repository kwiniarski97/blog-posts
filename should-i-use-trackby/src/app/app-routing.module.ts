import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WithtrackComponent } from './withtrack/withtrack.component';
import { WithouttrackComponent } from './withouttrack/withouttrack.component';

const routes: Routes = [
    { path: 'trackby', component: WithtrackComponent },
    { path: 'nontrackby', component: WithouttrackComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
